package com.springdatajpademo.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springdatajpademo.model.Skill;

@Repository
public interface SkillRepository extends CrudRepository<Skill, String> {
	
	List<Skill> findByTeacherId(Integer teacherId);
	
}
