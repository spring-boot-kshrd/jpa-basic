package com.springdatajpademo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.springdatajpademo.model.Teacher;
import com.springdatajpademo.service.TeacherService;

@RestController
@RequestMapping("teachers")
public class TeacherController {
	
	private TeacherService teacherService;
	
	@Autowired
	public void setTeacherService(TeacherService teacherService) {
		this.teacherService = teacherService;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Map<String, List<Teacher>>> findAll() {
		
		List<Teacher> teachers = teacherService.getAllTeachers(); 
		Map<String,List<Teacher>> data = new HashMap<String, List<Teacher>>();
		data.put("DATA", teachers);
		
		return ResponseEntity.ok(data); 
		
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Map<String, Teacher>> findOne(@PathVariable("id") Integer id) {
		
		Map<String, Teacher> data = new HashMap<String, Teacher>();
		
		data.put("DATA", teacherService.getTeacherById(id));
		
		return ResponseEntity.ok(data);
		
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, Teacher>> addOne(@RequestBody Teacher teacher) {

		Map<String, Teacher> data = new HashMap<String, Teacher>();
		Teacher t = null;
		
		if (teacher != null)
			t = teacherService.addTeacher(teacher);
		
		if (t != null)
			data.put("DATA", t);
		
		return ResponseEntity.ok(data);
		
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Map<String, Teacher>> deleteById(@PathVariable("id") Integer id) {
		
		Map<String, Teacher> data = new HashMap<String, Teacher>();
 		Teacher t = teacherService.getTeacherById(id);
		
		teacherService.deleteTeacher(id);
		
		if (teacherService.getTeacherById(id) == null) {
			data.put("DATA", t);
		}
		
		return ResponseEntity.ok(data);
		
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Map<String, Teacher>> updateById(@PathVariable("id") Integer id, @RequestBody Teacher teacher) {
		
		Map<String, Teacher> data = new HashMap<String, Teacher>();
		
		teacher.setId(id);
		
		Teacher t = teacherService.updateTeacher(teacher);
		
		data.put("DATA", t);
		
		return ResponseEntity.ok(data);
		
	}
	
	
	
	
}
