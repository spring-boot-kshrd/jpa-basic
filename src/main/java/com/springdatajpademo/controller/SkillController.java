package com.springdatajpademo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.springdatajpademo.model.Skill;
import com.springdatajpademo.service.SkillService;

@RestController
@RequestMapping("skills")
public class SkillController {
	
	private SkillService skillService;
	
	@Autowired
	public void setSkillService(SkillService skillService) {
		this.skillService = skillService;
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> findAllSkills(@RequestParam("teacher_id") Integer teacherId) {
		
		Map<String, Object> data = new HashMap<String, Object>();
		
		List<Skill> skills = skillService.findAllSkills(teacherId);
		
		data.put("DATA", skills);
		
		return ResponseEntity.ok(data);
		
	}

}
