package com.springdatajpademo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springdatajpademo.model.Teacher;
import com.springdatajpademo.repo.TeacherRepository;

@Service
public class TeacherService {
	
	private TeacherRepository teacherRepository;
	
	@Autowired 
	public void setTeacherRepository(TeacherRepository teacherRepository) {
		this.teacherRepository = teacherRepository;
	}
	
	public List<Teacher> getAllTeachers() {
		//return teachers;
		List<Teacher> teachers = new ArrayList<Teacher>();
		teacherRepository.findByOrderByNameAsc().forEach(teachers::add);
		return teachers;
	}
	
	public Teacher getTeacherById(int id) {
		return teacherRepository.findById(id).orElse(null);
	}
	
	public Teacher addTeacher(Teacher teacher) {
		return teacherRepository.save(teacher);
	}
	
	public Teacher updateTeacher(Teacher teacher) {
		return teacherRepository.save(teacher);
	}
	
	public void deleteTeacher(int id) {
		teacherRepository.deleteById(id);
	}
	
}
