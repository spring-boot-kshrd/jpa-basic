package com.springdatajpademo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springdatajpademo.model.Skill;
import com.springdatajpademo.repo.SkillRepository;

@Service
public class SkillService {

	private SkillRepository skillRepository;
	
	@Autowired
	public void setSkillRepository(SkillRepository skillRepository) {
		this.skillRepository = skillRepository;
	}
	
	public List<Skill> findAllSkills(Integer teacherId) {
		
		List<Skill> skills = new ArrayList<Skill>();
		
		skills = skillRepository.findByTeacherId(teacherId);
		
		return skills;
		
	}
	
}
